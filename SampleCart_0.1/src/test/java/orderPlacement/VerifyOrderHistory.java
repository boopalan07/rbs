/**
 * 
 */
package orderPlacement;

import pages.HomePage;
import pages.MyAccountPage;

public class VerifyOrderHistory {

    public void verifyOrderHistory() throws Exception {
        try {
            HomePage.navigateToAccountPage();
            MyAccountPage.verifyOrderHistory();
        } catch (Exception e) {
            throw new RuntimeException("Error in verifyinig Order history" + e.getMessage());
        }
    }
}
