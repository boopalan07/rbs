/**
 * 
 */
package orderPlacement;

import pages.AddressesPage;
import pages.CartPage;
import pages.CartSummaryPage;
import pages.HomePage;
import pages.OrderConfirmationPage;
import pages.PaymentMethodPage;
import pages.ShippingPage;

public class OrderTshirts {

    public void placeOrder() throws Exception {
        try {
            HomePage.addTshirtToCart();
            CartPage.addToCart();
            CartSummaryPage.proceedToCheckout();
            AddressesPage.proceedToCheckout();
            ShippingPage.proceedToCheckout();
            PaymentMethodPage.bankWirePayment();
            OrderConfirmationPage.verifyOrderConfirmation();
        } catch (Exception e) {
            throw new RuntimeException("Can not place an Order" + e.getMessage());
        }
    }
}
