/**
 * 
 */
package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import utilities.CommonFunctions;

public class PaymentMethodPage {

    static WebDriver driver = HomePage.driver;
    static By paymentMethodPageElement = By.xpath(
            "//h1[contains(text(),'Please choose your payment method')]");
    static By btnOrderConfirm = By.xpath("//button//span[contains(text(),'I confirm my order')]");
    static By btnBankWirePymt = By.xpath("//a[@class='bankwire']");
    static By btnChequeWirePymt = By.xpath("//a[@class='cheque']");

    public static void navigateToHomePage() throws Exception {
        CommonFunctions.clickOnElement(HomePage.imgHomePage);
    }

    public static void isPageDisplayed() throws Exception {

        try {
            CommonFunctions.waitForPageLoad(paymentMethodPageElement);
        } catch (Exception e) {
            throw new Exception("Payment method page can not be loaded " + e.getMessage());
        }
    }

    public static void bankWirePayment() throws Exception {

        try {
            isPageDisplayed();
            CommonFunctions.clickOnElement(btnBankWirePymt);
            CommonFunctions.clickOnElement(btnOrderConfirm);
        } catch (Exception e) {
            throw new Exception("Payment method page can not be loaded " + e.getMessage());
        }
    }

    public static void chequePayment() throws Exception {

        try {
            isPageDisplayed();
            CommonFunctions.clickOnElement(btnChequeWirePymt);
            CommonFunctions.clickOnElement(btnOrderConfirm);
        } catch (Exception e) {
            throw new Exception("Payment method page can not be loaded " + e.getMessage());
        }
    }

}
