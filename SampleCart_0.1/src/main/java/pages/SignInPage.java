/**
 * 
 */
package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utilities.CommonFunctions;

public class SignInPage {

    static WebDriver driver = HomePage.driver;
    static WebElement element;
    static By textEmailAddress = By.xpath("//input[@id='email']");
    static By textPassword = By.xpath("//input[@id='passwd']");
    static By btnSignIn = By.xpath("//button[@id='SubmitLogin']");

    public static void navigateToHomePage() throws Exception {
        CommonFunctions.clickOnElement(HomePage.imgHomePage);
    }

    public static void isPageDisplayed() throws Exception {

        try {
            CommonFunctions.waitForPageLoad(btnSignIn);
        } catch (Exception e) {
            throw new Exception("Page can not be loaded " + e.getMessage());
        }
    }

    public static void signIn(String userId, String password) throws Exception {
        try {
            isPageDisplayed();
            CommonFunctions.enterTextInTextField(textEmailAddress, userId);
            CommonFunctions.enterTextInTextField(textPassword, password);
            CommonFunctions.clickOnElement(btnSignIn);
        } catch (Exception e) {
            throw new Exception("Sign in is not success " + e.getMessage());
        }
    }
}
