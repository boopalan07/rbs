/**
 * 
 */
package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import utilities.CommonFunctions;

public class ShippingPage {

    static WebDriver driver = HomePage.driver;
    static By shippingPageElement = By.xpath("//h1[contains(text(),'Shipping')]");
    static By btnProceedToCheckOut = By.xpath(
            "//button//span[contains(text(),'Proceed to checkout')]");
    static By termsAndAggreementCheckBox = By.xpath("//input[@id='cgv']");

    public static void navigateToHomePage() throws Exception {
        CommonFunctions.clickOnElement(HomePage.imgHomePage);
    }

    public static void isPageDisplayed() throws Exception {

        try {
            CommonFunctions.waitForPageLoad(shippingPageElement);
        } catch (Exception e) {
            throw new Exception("Shipping page can not be loaded " + e.getMessage());
        }
    }

    public static void proceedToCheckout() throws Exception {
        try {
            isPageDisplayed();
            CommonFunctions.checkCheckBox(termsAndAggreementCheckBox);
            CommonFunctions.clickOnElement(btnProceedToCheckOut);
        } catch (Exception e) {
            throw new Exception("Can not be checked out from Shipping Page " + e.getMessage());
        }
    }
}
