/**
 * 
 */
package pages;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import utilities.CommonFunctions;
import utilities.DataManager;

public class CartPage {

    static WebDriver driver = HomePage.driver;
    static By imgCart = By.linkText("View my shopping cart");
    static By mssgProductConfirmation = By.xpath("//i[@class='icon-ok']/parent::h2");
    static By btnProceedToCheckOut = By.xpath("//a[@title='Proceed to checkout']");

    public void navigateToHomePage() throws Exception {
        CommonFunctions.clickOnElement(HomePage.imgHomePage);
    }

    public static void isPageDisplayed() throws Exception {

        try {
            CommonFunctions.waitForPageLoad(imgCart);
        } catch (Exception e) {
            throw new Exception("Page can not be loaded " + e.getMessage());
        }
    }

    public static void addToCart() throws Exception {

        try {
            isPageDisplayed();
            boolean result = CommonFunctions.verifyMessage(
                    mssgProductConfirmation,
                    DataManager.getConfigData("productAddedToCart"));
            assertTrue("Error in adding product to CART", result);
            CommonFunctions.clickOnElement(btnProceedToCheckOut);

        } catch (Exception e) {
            throw new Exception("Product can not be added to cart " + e.getMessage());
        }
    }

}
