/**
 * 
 */
package pages;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utilities.CommonFunctions;

public class MyAccountPage {

    static WebDriver driver = HomePage.driver;
    static WebElement element;
    static By myAccountPageElement = By.xpath("//h1[contains(text(),'My account')]");
    static By btnOrderHistory = By.linkText("Order history and details");
    static By userNameElement = By.xpath("//a[@class='account']/span");
    static By orderReferenceList = By.xpath("//table[@id='order-list']//tr//td[1]");

    public static String orederRefertenceNo;

    public void navigateToHomePage() throws Exception {
        CommonFunctions.clickOnElement(HomePage.imgHomePage);
    }

    public static void isPageDisplayed() throws Exception {

        try {
            CommonFunctions.waitForPageLoad(myAccountPageElement);
        } catch (Exception e) {
            throw new Exception("Page can not be loaded " + e.getMessage());
        }
    }

    public static void verifyUser(String userName) throws Exception {

        try {
            CommonFunctions.waitForPageLoad(myAccountPageElement);
            element = driver.findElement(userNameElement);
            assertEquals(
                    element.getText().toString().toLowerCase(),
                    userName.toLowerCase(),
                    "Invalid user");
        } catch (Exception e) {
            throw new Exception("Error in verifying user " + e.getMessage());
        }
    }

    public static void verifyOrderHistory() throws Exception {
        try {
            isPageDisplayed();
            CommonFunctions.clickOnElement(btnOrderHistory);
            List<WebElement> orderReferenceWebElementlist = driver.findElements(
                    (By) orderReferenceList);
            List<String> orderReferenceStringList = new ArrayList<String>();
            for (WebElement orderRefElement : orderReferenceWebElementlist) {
                orderReferenceStringList.add(orderRefElement.getText().toString().trim());
            }
            assertTrue(
                    "Order is not present in the order History",
                    orderReferenceStringList.contains(OrderConfirmationPage.orderReferenceNo));
        } catch (Exception e) {
            throw new Exception("Error in verifying Orders " + e.getMessage());
        }
    }

}
