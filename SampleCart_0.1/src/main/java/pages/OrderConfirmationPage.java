/**
 * 
 */
package pages;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import utilities.CommonFunctions;

public class OrderConfirmationPage {

    static WebDriver driver = HomePage.driver;
    public static String orderReferenceNo;
    static By orderConfirmationPageElement = By.xpath(
            "//h1[contains(text(),'Order confirmation')]");
    static By successMessageElement = By.xpath("//div[@class='box']/p");
    static By orderMessageContext = By.xpath("//div[@class='box']");

    public static void navigateToHomePage() throws Exception {
        CommonFunctions.clickOnElement(HomePage.imgHomePage);
    }

    public static void isPageDisplayed() throws Exception {

        try {
            CommonFunctions.waitForPageLoad(orderConfirmationPageElement);
        } catch (Exception e) {
            throw new Exception("Order COnfirmation page page can not be loaded " + e.getMessage());
        }
    }

    public static void verifyOrderConfirmation() throws Exception {
        try {
            isPageDisplayed();
            assertTrue(
                    "Order is not completed successfully",
                    CommonFunctions.verifyMessage(
                            orderConfirmationPageElement,
                            "Your order on My Store is complete."));
            navigateToHomePage();
        } catch (Exception e) {
            throw new Exception("Error is order placement. please try again : " + e.getMessage());
        }
    }

    public static void getOrderReference() throws Exception {
        try {
            isPageDisplayed();
            String[] orderString = driver.findElement(orderMessageContext).getText().toString()
                    .split("your order reference ");
            orderReferenceNo = orderString[1].split(" in the subject of your")[0];
        } catch (Exception e) {
            throw new Exception("Error is extracting order reference number : " + e.getMessage());
        }
    }
}
