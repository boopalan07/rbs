/**
 * 
 */
package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import utilities.CommonFunctions;

public class AddressesPage {

    static WebDriver driver = HomePage.driver;
    static By addressesPageElement = By.xpath("//h1[contains(text(),'Addresses')]");
    static By btnProceedToCheckOut = By.xpath(
            "//button//span[contains(text(),'Proceed to checkout')]");

    public void navigateToHomePage() throws Exception {
        CommonFunctions.clickOnElement(HomePage.imgHomePage);
    }

    public static void isPageDisplayed() throws Exception {

        try {
            CommonFunctions.waitForPageLoad(addressesPageElement);
        } catch (Exception e) {
            throw new Exception("Addresses page can not be loaded " + e.getMessage());
        }
    }

    public static void proceedToCheckout() throws Exception {
        try {
            isPageDisplayed();
            CommonFunctions.clickOnElement(btnProceedToCheckOut);
        } catch (Exception e) {
            throw new Exception("Can not be checkout from Addresses page " + e.getMessage());
        }
    }

}
