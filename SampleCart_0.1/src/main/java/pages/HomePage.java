package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import core.InitializeDriver;
import utilities.CommonFunctions;
import utilities.DataManager;

public class HomePage {

    static WebDriver driver = InitializeDriver.driver;
    static WebElement element;
    static By imgHomePage = By.id("header_logo");
    static By btnSignIn = By.xpath("//a[contains(text(),'Sign in')]");
    static By tShirtHeaderElement = By.xpath("(//li[@class='sfHoverForce']/a)[2]");
    static By tShirtProductElement = By.xpath("//a[@title='Faded Short Sleeve T-shirts']");
    static By btnAddToCart = By.xpath("//span[contains(text(),'Add to cart')]");

    public static void isPageDisplayed() throws Exception {

        try {
            CommonFunctions.waitForPageLoad(imgHomePage);
        } catch (Exception e) {
            throw new Exception("Page can not be loaded " + e.getMessage());
        }
    }

    public static void navigateToSignInPage() throws Exception {
        try {
            CommonFunctions.waitForPageLoad(btnSignIn);
            CommonFunctions.clickOnElement(btnSignIn);
        } catch (Exception e) {
            throw new Exception("Page can not be loaded " + e.getMessage());
        }
    }

    public static void addTshirtToCart() throws Exception {

        try {
            CommonFunctions.clickOnElement(tShirtHeaderElement);
            CommonFunctions.clickOnElement(tShirtProductElement);
            CommonFunctions.clickOnElement(btnAddToCart);
        } catch (Exception e) {
            throw new Exception("Product Can not be added to cart " + e.getMessage());
        }
    }

    public static void navigateToAccountPage() throws Exception {
        By usernameElement = By.xpath(
                "//a/span[contains(text(),'" + DataManager.getConfigData("userName") + "')]");
        try {
            isPageDisplayed();
            CommonFunctions.clickOnElement(usernameElement);
        } catch (Exception e) {
            throw new Exception("Account page can not be navigated " + e.getMessage());
        }
    }
}
