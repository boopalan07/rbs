/**
 * 
 */
package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utilities.CommonFunctions;

public class OrderHistoryPage {

    static WebDriver driver = HomePage.driver;
    static WebElement element;
    static By orderHistoryPageElement = By.xpath("//h1[contains(text(),'Order history')]");

    public static void navigateToHomePage() throws Exception {
        CommonFunctions.clickOnElement(HomePage.imgHomePage);
    }

    public static void isPageDisplayed() throws Exception {

        try {
            CommonFunctions.waitForPageLoad(orderHistoryPageElement);
        } catch (Exception e) {
            throw new Exception("Order history page can not be loaded " + e.getMessage());
        }
    }

    public static void verifyOrder() throws Exception {

    }

}
