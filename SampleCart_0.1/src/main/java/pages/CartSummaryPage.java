/**
 * 
 */
package pages;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import utilities.CommonFunctions;
import utilities.DataManager;

public class CartSummaryPage {

    static WebDriver driver = HomePage.driver;
    static By cartSummaryPageElement = By.id("cart_title");
    static By deleiveryAddressUserName = By.xpath(
            "//ul[@class='address first_item item box']//span[@class='address_name']");
    static By invoiceAddressUserName = By.xpath(
            "//ul[@class='address last_item alternate_item box']//span[@class='address_name']");
    static By btnProceedToCheckOut = By.xpath("//p//a[@title='Proceed to checkout']");

    public void navigateToHomePage() throws Exception {
        CommonFunctions.clickOnElement(HomePage.imgHomePage);
    }

    public static void isPageDisplayed() throws Exception {
        try {
            CommonFunctions.waitForPageLoad(cartSummaryPageElement);
        } catch (Exception e) {
            throw new Exception("Cart Summary page can not be loaded " + e.getMessage());
        }
    }

    public static void proceedToCheckout() throws Exception {
        try {
            isPageDisplayed();
            assertTrue(
                    "User name is invalid please verify the user name and try again later",
                    CommonFunctions.verifyMessage(
                            deleiveryAddressUserName,
                            DataManager.getConfigData("userName")));
            assertTrue(
                    "User name is invalid please verify the user name and try again later",
                    CommonFunctions.verifyMessage(
                            invoiceAddressUserName,
                            DataManager.getConfigData("userName")));
            CommonFunctions.clickOnElement(btnProceedToCheckOut);

        } catch (Exception e) {
            throw new Exception("Can not be checked out from Cart Summary Page " + e.getMessage());
        }
    }

}
