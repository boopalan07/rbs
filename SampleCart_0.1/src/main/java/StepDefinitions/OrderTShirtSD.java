/**
 * 
 */
package StepDefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import orderPlacement.OrderTshirts;
import orderPlacement.VerifyOrderHistory;
import pages.HomePage;
import pages.SignInPage;
import utilities.DataManager;

public class OrderTShirtSD {

    @Given("^Launch Sample Cart Application$")
    public void launchApplpication() throws Exception {
        HomePage.navigateToSignInPage();
        SignInPage.signIn(
                DataManager.getConfigData("userEmail"),
                DataManager.getConfigData("userPassword"));
        SignInPage.navigateToHomePage();

    }

    @When("^Order a T-Shirt$")
    public void placeATshirtOrder() throws Exception {
        OrderTshirts orderTShirts = new OrderTshirts();
        orderTShirts.placeOrder();
    }

    @Then("^Verify the order in Order History$")
    public void verifyOrderHistory() throws Exception {
        VerifyOrderHistory verifyOrders = new VerifyOrderHistory();
        verifyOrders.verifyOrderHistory();
    }

}
