/**
 * 
 */
package runner;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import core.InitializeDriver;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty",
                           "html:target/html-report",
                           "json:target/cucumber.json"}, features = {"Features"}, glue = {"src\\main\\java\\StepDefinitions"})
public class TestRunner {

    @BeforeClass
    public static void createSession() throws Exception {
        InitializeDriver.launchURL();
    }

    @AfterClass
    public static void closeSession() throws Exception {
        InitializeDriver.tearDown();
    }

}
